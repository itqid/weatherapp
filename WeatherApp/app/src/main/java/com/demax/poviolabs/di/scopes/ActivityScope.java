package com.demax.poviolabs.di.scopes;

import javax.inject.Scope;

/**
 * Created by demax on 3/7/16.
 */
@Scope
public @interface ActivityScope {
}
