package com.demax.poviolabs.interactors.interfaces;

import com.demax.poviolabs.interactors.listeners.OnFindCityListener;

/**
 * Created by demax on 3/7/16.
 */
public interface FindCityInteractor {
    public void getWeather(String _cityName, final OnFindCityListener _listener);
    public void cancel();
}
