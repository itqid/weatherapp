package com.demax.poviolabs.di.modules;

import com.demax.poviolabs.di.scopes.ActivityScope;
import com.demax.poviolabs.interactors.FindCityInteractorImpl;
import com.demax.poviolabs.interactors.interfaces.FindCityInteractor;
import com.demax.poviolabs.presenters.AddCityPresenterImpl;
import com.demax.poviolabs.presenters.interfaces.AddCityPresenter;
import com.demax.poviolabs.view.ui.activities.interfaces.AddCityView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by demax on 3/7/16.
 */
@Module
public class AddCityModule {

    AddCityView mView;

    public AddCityModule(AddCityView _view) {
        this.mView = _view;
    }

    @Provides
    @ActivityScope
    public AddCityView provideView(){
        return mView;
    }

    @Provides
    @ActivityScope
    FindCityInteractor provideInteractor(FindCityInteractorImpl _interactor){
        return _interactor;
    }

    @Provides
    @ActivityScope
    public AddCityPresenter providePresenter(AddCityView _view, FindCityInteractor _interactor){
        return new AddCityPresenterImpl(_view, _interactor);
    }
}