package com.demax.poviolabs.view.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.demax.poviolabs.R;
import com.demax.poviolabs.model.Weather;
import com.demax.poviolabs.model.WeatherState;

import org.parceler.Parcels;

import java.util.Iterator;

import butterknife.Bind;
import butterknife.ButterKnife;

public class WeatherDetailsActivity extends AppCompatActivity {

    @Bind(R.id.tv_city_name)
    TextView mTvCityName;
    @Bind(R.id.tv_temperature)
    TextView mTvTemperature;
    @Bind(R.id.tv_humidity)
    TextView mTvHumidity;
    @Bind(R.id.tv_description)
    TextView mTvDescription;
    @Bind(R.id.tv_no_data)
    TextView mTvNoData;
    @Bind(R.id.ll_info_holder)
    LinearLayout mLlInfoHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        if(getIntent() != null){
            mLlInfoHolder.setVisibility(View.VISIBLE);
            mTvNoData.setVisibility(View.GONE);
            Parcelable dataP;
            dataP = getIntent().getParcelableExtra("data");
            WeatherState weatherResponse;
            weatherResponse = Parcels.unwrap(dataP);
            setTitle("Weather Detail for " + weatherResponse.getCityName());
            mTvCityName.setText(getString(R.string.city_name, weatherResponse.getCityName()));
            mTvTemperature.setText(getString(R.string.temperature, weatherResponse.getMainParams().getTemperature().toString()));
            mTvHumidity.setText(getString(R.string.humidity, weatherResponse.getMainParams().getHumidity().toString()));
            StringBuilder stringBuilder = new StringBuilder();
            Iterator<Weather> iterator;
            iterator = weatherResponse.getWeather().iterator();
            while(iterator.hasNext()){
                String description;
                description = iterator.next().getDescription();
                stringBuilder.append(description);
                if(iterator.hasNext())
                    stringBuilder.append(",");
            }
            mTvDescription.setText(getString(R.string.description, stringBuilder.toString()));


        }else {
            mLlInfoHolder.setVisibility(View.GONE);
            mTvNoData.setVisibility(View.VISIBLE);
        }
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, WeatherDetailsActivity.class);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
