package com.demax.poviolabs.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demax.poviolabs.R;
import com.demax.poviolabs.model.WeatherState;

import java.util.List;

/**
 * Created by demax on 3/7/16.
 */
public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.ViewHolder>{
    private List<WeatherState> mWeatherList;
    public WeatherListAdapter(List<WeatherState> _data) {
        mWeatherList = _data;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mCityName;
        public TextView mTemperature;

        public ViewHolder(View itemView) {
            super(itemView);

            mCityName = (TextView) itemView.findViewById(R.id.tv_city_name);
            mTemperature = (TextView) itemView.findViewById(R.id.tv_temperature);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_recycler_view, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WeatherState item;
        item = mWeatherList.get(position);
        holder.mCityName.setText(item.mCityName);
        holder.mTemperature.setText(item.mMainParams.mTemperature.toString());
    }

    @Override
    public int getItemCount() {
        return mWeatherList.size();
    }
}
