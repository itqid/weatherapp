package com.demax.poviolabs.presenters;

import com.demax.poviolabs.interactors.interfaces.FindCityInteractor;
import com.demax.poviolabs.interactors.listeners.OnFindCityListener;
import com.demax.poviolabs.model.WeatherState;
import com.demax.poviolabs.presenters.interfaces.AddCityPresenter;
import com.demax.poviolabs.view.ui.activities.interfaces.AddCityView;

/**
 * Created by demax on 3/7/16.
 */
public class AddCityPresenterImpl implements AddCityPresenter{

    AddCityView mView;
    FindCityInteractor mInteractor;


    public AddCityPresenterImpl(AddCityView _view, FindCityInteractor _interactor) {
        this.mView = _view;
        this.mInteractor = _interactor;
    }

    @Override
    public void getWeatherForCity(String _cityName) {
        mInteractor.getWeather(_cityName, new OnFindCityListener() {
            @Override
            public void onSuccess(WeatherState _data) {
                mView.showResult(_data);
            }

            @Override
            public void onErroor(String _errorMessage) {
                mView.showErrorMessage(_errorMessage);
            }
        });
    }

    @Override
    public void destroy() {
        mView = null;
        mInteractor.cancel();
    }
}
