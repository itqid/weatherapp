package com.demax.poviolabs.view.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.demax.poviolabs.R;
import com.demax.poviolabs.common.ItemClickSupport;
import com.demax.poviolabs.di.components.DaggerListItemsComponent;
import com.demax.poviolabs.di.modules.ListItemsModule;
import com.demax.poviolabs.model.WeatherState;
import com.demax.poviolabs.presenters.interfaces.ListItemsPresenter;
import com.demax.poviolabs.utils.Navigator;
import com.demax.poviolabs.view.adapters.WeatherListAdapter;
import com.demax.poviolabs.view.ui.activities.interfaces.LoadDataView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CitiesTempActivity extends BaseActivity implements LoadDataView{
    private static final String TAG = CitiesTempActivity.class.getSimpleName();

    public static final String PARCEL_KEY = "result";
    @Inject
    public ListItemsPresenter mPresenter;
    @Bind(R.id.rv_weather_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.fab)
    FloatingActionButton mFabButton;
    @Bind(R.id.tv_empty_view)
    TextView mEmptyView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    WeatherListAdapter mAdapter;
    List<WeatherState> mWeatherList;
    Snackbar mSnackbar;
    private int mPositionRemovedItem;
    WeatherState mRemovedItem;

    private boolean isThereDatabaseAction = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities_temp);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setVisibility(View.INVISIBLE);
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        mPresenter.showItemDetails(mWeatherList.get(position));
                    }
                }
        );

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // If there is no items unable refreshing
                if (mEmptyView.getVisibility() == View.VISIBLE) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    return;
                }
                if(!isThereDatabaseAction){
                    mPresenter.refreshData();
                }

                isThereDatabaseAction = true;
            }
        });

        mPresenter.loadData();
    }

    @Override
    protected void setupComponent() {
        DaggerListItemsComponent.builder().listItemsModule(new ListItemsModule(this)).build().inject(this);
    }

    @OnClick(R.id.fab)
    public void onFabButtonClick(){
        Navigator.navigateToAddCity(CitiesTempActivity.this);
    }
    @Override
    public void showErrorMessage(String _errorMessage) {
        mSwipeRefreshLayout.setRefreshing(false);
        Toast.makeText(CitiesTempActivity.this, _errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void renderData(List<WeatherState> _data) {
        if(_data.isEmpty()){
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }else{
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        }

        if(mWeatherList == null){
            mWeatherList = new ArrayList<>();
            mWeatherList.addAll(_data);
            mAdapter = new WeatherListAdapter(mWeatherList);
            ItemTouchHelper.Callback callback = new TouchHelper(mAdapter);
            ItemTouchHelper helper = new ItemTouchHelper(callback);
            helper.attachToRecyclerView(mRecyclerView);
            mRecyclerView.setAdapter(mAdapter);
            return;
        }

        mWeatherList.clear();
        mWeatherList.addAll(_data);
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);

        isThereDatabaseAction = false;
    }

    @Override
    public void notifyForChanges(WeatherState _item, int _position, int _action) {
        if(_action == 1){ // Action : add item
            mWeatherList.add(_item);
            if(mWeatherList.size() == 1){
                mRecyclerView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
            }
            mAdapter.notifyDataSetChanged();
        }else {// Action : delete item
            if(mWeatherList.isEmpty()){
                mRecyclerView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
            }
        }

        isThereDatabaseAction = false;
    }

    @Override
    public void showDetailsForItem(WeatherState _item) {
        Navigator.navigateWeatherDetails(CitiesTempActivity.this, _item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Parcelable result = data.getParcelableExtra(PARCEL_KEY);
                WeatherState weatherResponse;
                weatherResponse = Parcels.unwrap(result);
                mPresenter.addItem(weatherResponse, mWeatherList.size());
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        mPresenter.destroy();
        mWeatherList.clear();
        mPresenter = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cities_temp, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public class TouchHelper extends ItemTouchHelper.SimpleCallback {

        WeatherListAdapter mAdapter;
        public TouchHelper(WeatherListAdapter _adapter){
            super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            this.mAdapter = _adapter;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            if(!isThereDatabaseAction){
                if(mSnackbar != null && mSnackbar.isShown()){
                    mSnackbar.dismiss();
                }
                mSnackbar = Snackbar.make(viewHolder.itemView, getString(R.string.delete_question), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.undo_title), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mRemovedItem != null) {
                                    mWeatherList.add(mPositionRemovedItem, mRemovedItem);
                                    mRemovedItem = null;
                                    mAdapter.notifyItemInserted(mPositionRemovedItem);
                                }
                            }
                        }).setCallback(new Snackbar.Callback() {
                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (event == DISMISS_EVENT_MANUAL || event == DISMISS_EVENT_SWIPE || event == DISMISS_EVENT_TIMEOUT) {
                                    isThereDatabaseAction = true;
                                    mPresenter.deleteItem(mPositionRemovedItem);
                                    return;
                                }
                            }
                        });
                mSnackbar.show();
                mRemovedItem = mWeatherList.get(viewHolder.getAdapterPosition());
                mPositionRemovedItem = viewHolder.getAdapterPosition();
                mWeatherList.remove(viewHolder.getAdapterPosition());
                mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        }
    }

}
