package com.demax.poviolabs.di.modules;

import android.app.Application;

import com.demax.poviolabs.common.ItemTypeAdapterFactory;
import com.demax.poviolabs.database.StorageManager;
import com.demax.poviolabs.network.WeatherService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by demax on 3/7/16.
 */
@Module
public class AppModule {
    public static final int NETWORK_TIMEOUT_SECONDS = 10;
    public static final String API_URL = "http://api.openweathermap.org";
    public static final String API_KEY = "c9a80d4c7f2eda8e617938711d4832b9";
    private Application mApplication;

    public AppModule(Application _application){
        this.mApplication = _application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application){
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapterFactory(new ItemTypeAdapterFactory());
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(Cache cache) {
        OkHttpClient client = new OkHttpClient();
        client.setCache(cache);
        client.setConnectTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        client.setReadTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        return client;
    }

    @Provides
    @Singleton
    WeatherService provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(API_URL)
                .client(okHttpClient)
                .build();

        return retrofit.create(WeatherService.class);
    }

    @Provides
    @Singleton
    public StorageManager provideStorageManager(Application _application, Gson _gson){
        return new StorageManager(_application, _gson);
    }
}
