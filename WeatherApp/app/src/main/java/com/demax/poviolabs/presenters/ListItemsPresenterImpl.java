package com.demax.poviolabs.presenters;

import com.demax.poviolabs.interactors.interfaces.ListItemsInteractor;
import com.demax.poviolabs.interactors.listeners.OnAddItemListener;
import com.demax.poviolabs.interactors.listeners.OnDeleteItemListener;
import com.demax.poviolabs.interactors.listeners.OnLoadDataListener;
import com.demax.poviolabs.interactors.listeners.OnRefreshDataListener;
import com.demax.poviolabs.model.WeatherState;
import com.demax.poviolabs.presenters.interfaces.ListItemsPresenter;
import com.demax.poviolabs.view.ui.activities.interfaces.LoadDataView;

import java.util.List;

/**
 * Created by demax on 3/7/16.
 */
public class ListItemsPresenterImpl implements ListItemsPresenter {

    private LoadDataView mView;
    private ListItemsInteractor mInteractor;

    public ListItemsPresenterImpl(LoadDataView _view, ListItemsInteractor _interactor) {
        this.mView = _view;
        this.mInteractor = _interactor;
    }

    @Override
    public void loadData() {
        mInteractor.loadData(new OnLoadDataListener() {
            @Override
            public void onSuccess(List<WeatherState> _data) {
                mView.renderData(_data);
            }

            @Override
            public void onError() {
                mView.showErrorMessage("");
            }
        });
    }

    @Override
    public void deleteItem(final int _position) {
        mInteractor.deleteItem(_position, new OnDeleteItemListener() {
            @Override
            public void onSuccess() {
                mView.notifyForChanges(null, _position, 2);
            }

            @Override
            public void onError() {
                mView.showErrorMessage("");
            }
        });
    }

    @Override
    public void refreshData() {
        mInteractor.refreshData(new OnRefreshDataListener() {
            @Override
            public void onSuccess(List<WeatherState> _data) {
                mView.renderData(_data);
            }

            @Override
            public void onError() {
                mView.showErrorMessage("");
            }
        });
    }

    @Override
    public void showItemDetails(WeatherState _item) {
        mView.showDetailsForItem(_item);
    }

    @Override
    public void addItem(final WeatherState _item, final int _position) {
        mInteractor.addItem(_item, new OnAddItemListener() {
            @Override
            public void onSuccess() {
                mView.notifyForChanges(_item, _position, 1);
            }

            @Override
            public void onError() {
                mView.showErrorMessage("Internet connection problem. Please try to update later.");
            }
        });
    }

    @Override
    public void destroy() {
        mView = null;
        mInteractor.cancel();
        mInteractor = null;
    }
}
