package com.demax.poviolabs.common;

import android.app.Application;

import com.demax.poviolabs.database.StorageManager;
import com.demax.poviolabs.di.components.DaggerAppComponent;
import com.demax.poviolabs.di.modules.AppModule;
import com.demax.poviolabs.network.WeatherService;

import javax.inject.Inject;

/**
 * Created by demax on 3/7/16.
 */
public class WeatherApp extends Application {
    private static WeatherApp instance;

    @Inject
    protected WeatherService mWeatherService;
    @Inject
    protected StorageManager mStorageManager;

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(WeatherApp.this);
        DaggerAppComponent.builder().appModule(new AppModule(this)).build().inject(this);
    }

    private static void setInstance(WeatherApp _app){
        WeatherApp.instance = _app;
    }

    public static WeatherApp getInstance(){
        return instance;
    }

    public WeatherService getWeatherService(){
        return mWeatherService;
    }

    public StorageManager getStorageManager() {
        return mStorageManager;
    }
}
