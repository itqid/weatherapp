package com.demax.poviolabs.presenters.interfaces;

/**
 * Created by demax on 3/7/16.
 */
public interface AddCityPresenter {
    public void getWeatherForCity(String _cityName);
    public void destroy();
}
