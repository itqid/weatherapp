package com.demax.poviolabs.presenters.interfaces;

import com.demax.poviolabs.model.WeatherState;

/**
 * Created by demax on 3/7/16.
 */
public interface ListItemsPresenter extends BasePresenter{
    public void loadData();
    public void addItem(final WeatherState _item, final  int _position);
    public void deleteItem(final  int _position);
    public void refreshData();
    public void showItemDetails(WeatherState _item);
    public void destroy();
}
