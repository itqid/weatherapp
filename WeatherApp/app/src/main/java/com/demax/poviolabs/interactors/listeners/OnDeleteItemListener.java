package com.demax.poviolabs.interactors.listeners;

/**
 * Created by demax on 3/7/16.
 */
public interface OnDeleteItemListener {
    public void onSuccess();
    public void onError();
}
