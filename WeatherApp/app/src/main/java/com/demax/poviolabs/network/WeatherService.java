package com.demax.poviolabs.network;

import com.demax.poviolabs.model.WeatherState;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by demax on 3/7/16.
 */
public interface WeatherService {
    @GET("/data/2.5/weather")
    Call<WeatherState> getWeather(@Query("q") String _cityName, @Query("appid") String _apikey );
}
