package com.demax.poviolabs.view.ui.activities.interfaces;

import com.demax.poviolabs.model.WeatherState;

/**
 * Created by demax on 3/7/16.
 */
public interface AddCityView {
    public void showResult(WeatherState _result);
    public void showErrorMessage(String _errorMessage);
}
