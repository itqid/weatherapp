package com.demax.poviolabs.presenters.interfaces;

/**
 * Created by demax on 3/7/16.
 */
public interface BasePresenter {
    void destroy();
}
