package com.demax.poviolabs.interactors.listeners;

import com.demax.poviolabs.model.WeatherState;

/**
 * Created by demax on 3/7/16.
 */
public interface OnFindCityListener {
    public void onSuccess(WeatherState _data);
    public void onErroor(String _errorMessage);
}
