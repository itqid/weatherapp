package com.demax.poviolabs.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.demax.poviolabs.model.WeatherState;
import com.demax.poviolabs.view.ui.activities.AddCityActivity;
import com.demax.poviolabs.view.ui.activities.WeatherDetailsActivity;

import org.parceler.Parcels;

/**
 * Created by demax on 3/7/16.
 */
public class Navigator {

    public static void navigateToAddCity(Context context) {
        if (context != null) {
            Intent intentToLaunch = AddCityActivity.getCallingIntent(context);
            ((Activity)context).startActivityForResult(intentToLaunch,1);
        }
    }

    public static void navigateWeatherDetails(Context context, Object _data) {
        if (context != null) {
            Intent intentToLaunch = WeatherDetailsActivity.getCallingIntent(context);
            intentToLaunch.putExtra("data", Parcels.wrap(((WeatherState) _data)));
            context.startActivity(intentToLaunch);
        }
    }
}
