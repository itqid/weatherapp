package com.demax.poviolabs.interactors;

import com.demax.poviolabs.di.modules.AppModule;
import com.demax.poviolabs.interactors.interfaces.FindCityInteractor;
import com.demax.poviolabs.interactors.listeners.OnFindCityListener;
import com.demax.poviolabs.model.WeatherState;
import com.demax.poviolabs.network.WeatherService;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by demax on 3/7/16.
 */
public class FindCityInteractorImpl implements FindCityInteractor {

    WeatherService mService;
    private boolean isCanceled = false;
    Call<WeatherState> mCall;
    @Inject
    public FindCityInteractorImpl(WeatherService _service){
        mService = _service;
    }
    @Override
    public void getWeather(String _cityName, final OnFindCityListener _listener) {

        if(mCall != null){
            mCall.cancel();
        }
        mCall = mService.getWeather(_cityName, AppModule.API_KEY);
        mCall.enqueue(new Callback<WeatherState>() {
            @Override
            public void onResponse(Response<WeatherState> response, Retrofit retrofit) {
                if (!isCanceled){
                    _listener.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (!isCanceled){
                    _listener.onErroor(t.getMessage());
                }
            }
        });
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }
}
