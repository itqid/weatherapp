package com.demax.poviolabs.database;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.*;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.demax.poviolabs.interactors.listeners.OnLoadDataListener;
import com.demax.poviolabs.model.WeatherState;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by demax on 3/7/16.
 */
public class StorageManager {

    private Gson mGson;
    private SharedPreferences mSharedPreferences;
    private static final int TASK_ADD = 1;
    private static final int TASK_REMOVE = 2;
    private static final int TASK_GET_ALL = 3;

    private final static String TIMESTAMP_KEY = "timestamp";
    private final static String DATA_KEY = "data";
    private StorageTask mStorageTask;
    private Handler uiHandler;
    private OnLoadDataListener mOnLoadDataListener;

    @Inject
    public StorageManager(Application _application, Gson _gson) {
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(_application.getApplicationContext());
        this.mGson = _gson;
        this.uiHandler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case TASK_GET_ALL:
                        Bundle bundle;
                        bundle = msg.getData();
                        List<WeatherState> data;
                        data = Parcels.unwrap(bundle.getParcelable("list"));
                        if(mOnLoadDataListener != null){
                            if(data != null){
                                mOnLoadDataListener.onSuccess(data);
                            }else {
                                mOnLoadDataListener.onError();
                            }
                        }

                        break;
                }
            }
        };

        this.mStorageTask = getStorageTask();

    }

    private StorageTask getStorageTask(){
        if(mStorageTask == null){
            this.mStorageTask = new StorageTask(mSharedPreferences, mGson, uiHandler);
            mStorageTask.start();
        }

        return mStorageTask;
    }

    public void addEntry(WeatherState _entry){
        if (getStorageTask() != null){
            getStorageTask().addEntry(_entry);
        }
    }

    public void deleteEntry(int _position){
        if (getStorageTask() != null){
            getStorageTask().deleteEntry(_position);
        }
    }

    public void getAllEntries(OnLoadDataListener _listener){
        mOnLoadDataListener = _listener;
        if (getStorageTask() != null){
            getStorageTask().getAllEntries();
        }
    }

    public void clear(){
        if (getStorageTask() != null){
            getStorageTask().reset();
            getStorageTask().quit();
        }
    }

    /**
     * Its queue managed background task aimed to handle all database action sequently
     */
    private static class StorageTask extends HandlerThread{

        private Handler mHandler;
        private Gson mGson;
        private SharedPreferences mSharedPreferences;
        private Handler mUiHandler;

        public StorageTask(SharedPreferences _preferences, Gson _gson, Handler _uiHandler) {
            super(StorageTask.class.getSimpleName(), android.os.Process.THREAD_PRIORITY_BACKGROUND);
            this.mSharedPreferences = _preferences;
            mSharedPreferences.edit().putString(DATA_KEY, "");
            this.mGson = _gson;
            this.mUiHandler = _uiHandler;
        }

        @Override
        protected void onLooperPrepared() {
            super.onLooperPrepared();
            mHandler = new Handler(){
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what){
                        case TASK_ADD:
                            Bundle bundle = msg.getData();
                            WeatherState weatherState;
                            weatherState = Parcels.unwrap(bundle.getParcelable("data"));
                            addEntry(weatherState);
                            break;
                        case TASK_REMOVE:
                            deleteEntryAsync(msg.arg1);
                            break;
                        case TASK_GET_ALL:
                            getAllEntries();
                            break;
                    }
                }
            };
        }

        @Override
        public boolean quit() {
            mUiHandler = null;
            return super.quit();
        }

        public void deleteEntryAsync(int _position){
            Message message;
            message = mHandler.obtainMessage();
            message.arg1 = _position;
            message.what = TASK_REMOVE;
            mHandler.sendMessage(message);
        }

        public void addEntryAsync(WeatherState _entry){
            Message message;
            message = mHandler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putParcelable("data", Parcels.wrap(_entry));
            message.setData(bundle);
            message.what = TASK_REMOVE;
            mHandler.sendMessage(message);
        }

        public void getAllEntriesAsync(){
            Message message;
            message = mHandler.obtainMessage();
            message.what = TASK_GET_ALL;
            mHandler.sendMessage(message);
        }

        private void sendDataToUI(List<WeatherState> _list){
            if(mUiHandler != null){
                Message message;
                message = mUiHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putParcelable("list", Parcels.wrap(_list));
                message.setData(bundle);
                message.what = TASK_GET_ALL;
                mUiHandler.sendMessage(message);
            }
        }
        public void reset(){
            if(mUiHandler != null){
                mUiHandler.removeCallbacksAndMessages(null);
            }

            mHandler.removeCallbacksAndMessages(null);
        }

        private void serialize(@NonNull List<WeatherState> _data){
            DateFormat dateTimeFormater = SimpleDateFormat.getDateTimeInstance();
            mSharedPreferences.edit().putString(TIMESTAMP_KEY, dateTimeFormater.format(Calendar.getInstance().getTime())).commit();
            Iterator<WeatherState> iterator;
            iterator= _data.iterator();
            JSONArray jsonArray = new  JSONArray();
            while (iterator.hasNext()){
                WeatherState weatherResponse;
                weatherResponse = iterator.next();
                String rootObject = mGson.toJson(weatherResponse);
                jsonArray.put(rootObject);
            }
            mSharedPreferences.edit().putString(DATA_KEY, jsonArray.toString()).commit();
        }

        private @NonNull List<WeatherState> deserialize(){
            List<WeatherState> weatherResponseList = new ArrayList<>();

            String data;
            data = mSharedPreferences.getString(DATA_KEY, "");
            data = data.replace("\\", "");
            data = data.replace("\"{", "{");
            data = data.replace("}\"", "}");
            data = data.replace("\"[", "[");
            data = data.replace("]\"", "]");
            if (data.isEmpty()){
                return weatherResponseList;
            }else {
                try {
                    JSONArray rootObject = new JSONArray(data);
                    for (int i = 0; i < rootObject.length(); i++){
                        WeatherState weatherResponse;
                        JSONObject object;
                        object = rootObject.getJSONObject(i);
                        weatherResponse = mGson.fromJson(object.toString(), WeatherState.class);
                        weatherResponseList.add(weatherResponse);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return weatherResponseList;

        }

        private void getAllEntries(){
            List<WeatherState> data;
            data = deserialize();
            sendDataToUI(data);
        }



        private void addEntry(WeatherState _response){
            List<WeatherState> entrie = deserialize();
            entrie.add(_response);
            serialize(entrie);
        }

        private void deleteEntry(int pos){
            List<WeatherState> entrie = deserialize();
            if(entrie.size() <= pos) return;
            entrie.remove(pos);
            serialize(entrie);
        }
    }
}
