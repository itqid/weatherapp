package com.demax.poviolabs.di.modules;

import com.demax.poviolabs.di.scopes.ActivityScope;
import com.demax.poviolabs.interactors.ListItemsInteractorImpl;
import com.demax.poviolabs.interactors.interfaces.ListItemsInteractor;
import com.demax.poviolabs.presenters.interfaces.ListItemsPresenter;
import com.demax.poviolabs.presenters.ListItemsPresenterImpl;
import com.demax.poviolabs.view.ui.activities.interfaces.LoadDataView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by demax on 3/7/16.
 */
@Module
public class ListItemsModule {
    private LoadDataView mLoadDataView;

    public ListItemsModule(LoadDataView _view){
        this.mLoadDataView = _view;
    }

    @Provides
    @ActivityScope
    public LoadDataView provideView(){
        return mLoadDataView;
    }

    @Provides
    @ActivityScope
    public ListItemsInteractor provideInteractor(ListItemsInteractorImpl _interactor){
        return _interactor;
    }

    @Provides
    @ActivityScope
    public ListItemsPresenter providePresenter(LoadDataView _view, ListItemsInteractor _interactor){
        return new ListItemsPresenterImpl(_view, _interactor);
    }
}
