package com.demax.poviolabs.interactors.listeners;

import com.demax.poviolabs.model.WeatherState;

import java.util.List;

/**
 * Created by demax on 3/7/16.
 */
public interface OnRefreshDataListener {
    public void onSuccess(List<WeatherState> _data);
    public void onError();
}
