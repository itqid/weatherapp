package com.demax.poviolabs.view.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.demax.poviolabs.R;
import com.demax.poviolabs.di.components.DaggerAddCityComponent;
import com.demax.poviolabs.di.modules.AddCityModule;
import com.demax.poviolabs.model.WeatherState;
import com.demax.poviolabs.presenters.interfaces.AddCityPresenter;
import com.demax.poviolabs.view.ui.activities.interfaces.AddCityView;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCityActivity extends BaseActivity  implements AddCityView {

    @Bind(R.id.input_layout_name)
    TextInputLayout mInputLayout;
    @Bind(R.id.et_input_name)
    EditText mEtCityName;
    @Bind(R.id.btn_find)
    Button mBtnFind;
    @Bind(R.id.ll_info_holder)
    LinearLayout mLlInfoHolder;
    @Bind(R.id.tv_city_name)
    TextView mTvCityName;
    @Bind(R.id.tv_temperature)
    TextView mTvTemp;
    @Bind(R.id.btn_add)
    Button mBtnAdd;
    @Bind(R.id.tv_no_result)
    TextView mTvNoResult;
    @Bind(R.id.progress_bar)
    ProgressBar mProgressBar;
    @Inject
    AddCityPresenter mPresenter;
    WeatherState mResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        mBtnAdd.setEnabled(false);
        mBtnFind.setEnabled(false);
        mTvNoResult.setVisibility(View.VISIBLE);
        mLlInfoHolder.setVisibility(View.GONE);

        mEtCityName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                validateCityName();
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    protected void setupComponent() {
        DaggerAddCityComponent.builder().addCityModule(new AddCityModule(this)).build().inject(this);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void validateCityName(){
        if(mEtCityName.getText().toString().trim().isEmpty()){
            mInputLayout.setError(getString(R.string.invalid_name));
            mBtnFind.setEnabled(false);
            mBtnAdd.setEnabled(false);
            requestFocus(mEtCityName);
            mTvNoResult.setVisibility(View.VISIBLE);
            mLlInfoHolder.setVisibility(View.GONE);
        } else {
            mInputLayout.setErrorEnabled(false);
            mBtnFind.setEnabled(true);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_add)
    public void onAddButtonClicked(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra(CitiesTempActivity.PARCEL_KEY, Parcels.wrap(mResponse));
                setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @OnClick(R.id.btn_find)
    public void onFindButtonClicked(){
        mProgressBar.setVisibility(View.VISIBLE);
        mTvNoResult.setVisibility(View.GONE);
        hideSoftKeyboard();
        mPresenter.getWeatherForCity(mEtCityName.getText().toString().trim());
    }

    @Override
    public void showResult(WeatherState _result) {
        mProgressBar.setVisibility(View.GONE);
        mTvNoResult.setVisibility(View.GONE);
        mLlInfoHolder.setVisibility(View.VISIBLE);
        mResponse = _result;
        mTvCityName.setText(getString(R.string.city_name,_result.getCityName()));
        mTvTemp.setText(getString(R.string.temperature, _result.getMainParams().getTemperature().toString()));
        mBtnAdd.setEnabled(true);
    }

    @Override
    public void showErrorMessage(String _errorMessage) {

    }

    private void hideSoftKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(AddCityActivity.this);
        mPresenter.destroy();
        mPresenter = null;
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, AddCityActivity.class);
    }
}
