package com.demax.poviolabs.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by demax on 3/7/16.
 */
@Parcel
public class Weather {

    @SerializedName("id")
    private Integer mId;
    @SerializedName("description")
    private String mDescription;

    public Integer getId() {
        return mId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setId(Integer _id) {
        this.mId = _id;
    }

    public void setDescription(String _description) {
        this.mDescription = _description;
    }
}
