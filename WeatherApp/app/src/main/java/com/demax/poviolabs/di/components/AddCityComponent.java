package com.demax.poviolabs.di.components;

import com.demax.poviolabs.di.modules.AddCityModule;
import com.demax.poviolabs.di.modules.NetworkModule;
import com.demax.poviolabs.di.scopes.ActivityScope;
import com.demax.poviolabs.view.ui.activities.AddCityActivity;

import dagger.Component;

/**
 * Created by demax on 3/7/16.
 */
@ActivityScope
@Component(modules = {NetworkModule.class, AddCityModule.class})
public interface AddCityComponent {
    void inject(AddCityActivity _activity);
}

