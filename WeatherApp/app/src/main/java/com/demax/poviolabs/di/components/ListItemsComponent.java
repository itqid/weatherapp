package com.demax.poviolabs.di.components;

import com.demax.poviolabs.di.modules.ListItemsModule;
import com.demax.poviolabs.di.modules.NetworkModule;
import com.demax.poviolabs.di.scopes.ActivityScope;
import com.demax.poviolabs.view.ui.activities.CitiesTempActivity;

import dagger.Component;

/**
 * Created by demax on 3/7/16.
 */
@ActivityScope
@Component(modules = {NetworkModule.class, ListItemsModule.class})
public interface ListItemsComponent {
    void inject(CitiesTempActivity _activity);
}
