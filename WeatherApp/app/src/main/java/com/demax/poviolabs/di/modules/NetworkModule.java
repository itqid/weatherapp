package com.demax.poviolabs.di.modules;

import com.demax.poviolabs.common.WeatherApp;
import com.demax.poviolabs.database.StorageManager;
import com.demax.poviolabs.di.scopes.ActivityScope;
import com.demax.poviolabs.network.WeatherService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by demax on 3/7/16.
 */
@Module
public class NetworkModule {
    @Provides
    @ActivityScope
    public WeatherService provideWeatherService(){
        return WeatherApp.getInstance().getWeatherService();
    }

    @Provides
    @ActivityScope
    public StorageManager provideStorageManager(){
        return WeatherApp.getInstance().getStorageManager();
    }
}
