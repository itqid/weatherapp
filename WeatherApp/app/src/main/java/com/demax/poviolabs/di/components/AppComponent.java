package com.demax.poviolabs.di.components;

import com.demax.poviolabs.common.WeatherApp;
import com.demax.poviolabs.di.modules.AppModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by demax on 3/7/16.
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(WeatherApp _app);
}

