package com.demax.poviolabs.interactors;

import android.os.AsyncTask;

import com.demax.poviolabs.database.StorageManager;
import com.demax.poviolabs.di.modules.AppModule;
import com.demax.poviolabs.interactors.interfaces.ListItemsInteractor;
import com.demax.poviolabs.interactors.listeners.OnAddItemListener;
import com.demax.poviolabs.interactors.listeners.OnDeleteItemListener;
import com.demax.poviolabs.interactors.listeners.OnLoadDataListener;
import com.demax.poviolabs.interactors.listeners.OnRefreshDataListener;
import com.demax.poviolabs.model.WeatherState;
import com.demax.poviolabs.network.WeatherService;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import retrofit.Response;

/**
 * Created by demax on 3/7/16.
 */
public class ListItemsInteractorImpl  implements ListItemsInteractor{
    private static final String TAG = ListItemsInteractorImpl.class.getSimpleName();
    private WeatherService mWeatherService;
    private StorageManager mStorageManager;
    private RefreshDataTask mTask;

    private boolean isCanceled = false;

    @Inject
    public ListItemsInteractorImpl(WeatherService _weatherService, StorageManager _storageManager){
        this.mWeatherService = _weatherService;
        this.mStorageManager = _storageManager;
    }
    @Override
    public void addItem(WeatherState _item, OnAddItemListener _listener) {
        mStorageManager.addEntry(_item);
        _listener.onSuccess();
    }

    @Override
    public void deleteItem(int _position, OnDeleteItemListener _listener) {
        mStorageManager.deleteEntry(_position);
        _listener.onSuccess();
    }

    @Override
    public void loadData(OnLoadDataListener _listener) {
       mStorageManager.getAllEntries(_listener);
    }

    @Override
    public void refreshData(final OnRefreshDataListener _listener) {
        mStorageManager.getAllEntries(new OnLoadDataListener() {
            @Override
            public void onSuccess(List<WeatherState> _data) {
                clearTask();
                mTask = new RefreshDataTask(mWeatherService, _data, new OnRefreshDataListener() {
                    @Override
                    public void onSuccess(List<WeatherState> _data) {
                        if (!isCanceled) {
                            _listener.onSuccess(_data);
                        }
                    }

                    @Override
                    public void onError() {
                        if (!isCanceled) {
                            _listener.onError();
                        }
                    }
                });

                mTask.execute();
            }

            @Override
            public void onError() {
                _listener.onError();
            }
        });
    }

    @Override
    public void cancel() {
        isCanceled = true;
        clearTask();
        mWeatherService = null;
        mStorageManager.clear();
        mStorageManager = null;
    }

    private void clearTask(){
        if(mTask != null){
            mTask.cancel(true);
            mTask = null;
        }
    }

    /**
     * Task that handle getting weather state for the all instances
     */
    private static class RefreshDataTask extends AsyncTask<List<WeatherState>, Void, List<WeatherState>> {
        private WeakReference<WeatherService> mService;
        private List<WeatherState> mListOfCities;
        private OnRefreshDataListener mListener;

        public RefreshDataTask(WeatherService _service, List<WeatherState> _listOfCities, OnRefreshDataListener _listener) {
            this.mService = new WeakReference<>(_service);
            this.mListOfCities = _listOfCities;
            this.mListener = _listener;
        }

        @Override
        protected List<WeatherState> doInBackground(List<WeatherState>... params) {
            try {
                for (int i = 0; i < mListOfCities.size(); i++){
                    WeatherState weatherResponse;
                    weatherResponse = mListOfCities.get(i);
                    Response response;
                    response = mService.get().getWeather(weatherResponse.getCityName(), AppModule.API_KEY).execute();

                    // Update only returned values
                    if(!(response.code() > 200 && response.isSuccess())){
                        mListOfCities.set(i, weatherResponse);
                    }
                }

            }catch (IOException e) {
                e.printStackTrace();
                mListOfCities.clear();
            }

            return mListOfCities;
        }

        @Override
        protected void onPostExecute(List<WeatherState> _result) {
            super.onPostExecute(_result);
            try{
                if(_result.isEmpty()){
                    mListener.onError();
                }else{
                    mListener.onSuccess(_result);
                }
            }finally {
                if(mService != null){
                    mService.clear();
                }

                mListOfCities.clear();
                mListener = null;
            }


        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if(mService != null){
                mService.clear();
                mListOfCities.clear();
                mListener = null;
            }
        }
    }
}
