package com.demax.poviolabs.view.ui.activities.interfaces;

import com.demax.poviolabs.model.WeatherState;

import java.util.List;

/**
 * Created by demax on 3/7/16.
 */
public interface LoadDataView {
    public void showErrorMessage(String _errorMessage);
    public void renderData(List<WeatherState> _data);
    public void notifyForChanges(WeatherState _item, final int _position, int _action);
    public void showDetailsForItem(WeatherState _item);
}
