package com.demax.poviolabs.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by demax on 3/7/16.
 */
@Parcel
public class WeatherState {

    @SerializedName("id")
    public Integer mId;
    @SerializedName("main")
    public Main mMainParams;
    @SerializedName("weather")
    public ArrayList<Weather> mWeather;
    @SerializedName("name")
    public String mCityName;

    public Integer getId() {
        return mId;
    }

    public void setId(Integer _id) {
        this.mId = _id;
    }

    public Main getMainParams() {
        return mMainParams;
    }

    public void setMainParams(Main _mainParams) {
        this.mMainParams = _mainParams;
    }

    public ArrayList<Weather> getWeather() {
        return mWeather;
    }

    public void setWeather(ArrayList<Weather> _weather) {
        this.mWeather = mWeather;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String _cityName) {
        this.mCityName = _cityName;
    }
}
