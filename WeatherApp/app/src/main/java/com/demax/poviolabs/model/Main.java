package com.demax.poviolabs.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by demax on 3/7/16.
 */
@Parcel
public class Main {
    @SerializedName("temp")
    public Long mTemperature;
    @SerializedName("humidity")
    public Integer mHumidity;

    public Long getTemperature() {
        return mTemperature;
    }

    public void setTemperature(Long _temperature) {
        this.mTemperature = _temperature;
    }

    public Integer getHumidity() {
        return mHumidity;
    }

    public void setHumidity(Integer _humidity) {
        this.mHumidity = _humidity;
    }
}
