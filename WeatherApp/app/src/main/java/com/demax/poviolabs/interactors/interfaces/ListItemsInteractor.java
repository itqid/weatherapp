package com.demax.poviolabs.interactors.interfaces;

import com.demax.poviolabs.interactors.listeners.OnAddItemListener;
import com.demax.poviolabs.interactors.listeners.OnDeleteItemListener;
import com.demax.poviolabs.interactors.listeners.OnLoadDataListener;
import com.demax.poviolabs.interactors.listeners.OnRefreshDataListener;
import com.demax.poviolabs.model.WeatherState;

/**
 * Created by demax on 3/7/16.
 */
public interface ListItemsInteractor {
    public void addItem(WeatherState _item, OnAddItemListener _listener);
    public void deleteItem(int _position, OnDeleteItemListener _listener);
    public void loadData(OnLoadDataListener _listener);
    public void refreshData(final OnRefreshDataListener _listener);
    public void cancel();
}
